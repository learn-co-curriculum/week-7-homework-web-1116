## Week 7 Weekend Homework

It's the end of Week 7 - you made it! Awesome job. For homework this weekend.

1. We deployed a few more labs in the OO JS section. If you're feeling behind on labs, focus on the following sections.
	+ Higher Order Functions
	+ Object Oriented JavaScript

2. If you finish the labs, focus on the next part of the [You Don't Know JS](https://github.com/getify/You-Dont-Know-JS/blob/master/this%20%26%20object%20prototypes/README.md) book on `this` and object prototypes.  

3. Get some rest - next week we'll look at implementing some cool OO patterns into a HTML/CSS/JS project. 